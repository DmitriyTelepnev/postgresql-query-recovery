package signal

import (
	"os"
	"os/signal"
	"syscall"
)

type signalHandler struct {
	sig          chan os.Signal
	callbackFunc func() error
}

func NewHandler(f func() error) *signalHandler {
	sig := make(chan os.Signal)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)

	return &signalHandler{sig, f}
}

func (s *signalHandler) Poll() error {
	<-s.sig
	return s.callbackFunc()
}

package logger

type Logger interface {
	Info(msg string)
	Infof(pattern string, params ...interface{})
	Warn(msg string)
	Warnf(pattern string, params ...interface{})
	Debug(msg string)
	Debugf(pattern string, params ...interface{})
	Error(msg string) error
	Errorf(pattern string, params ...interface{}) error
	Fatal(msg string)
	Fatalf(pattern string, params ...interface{})
}

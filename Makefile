GO111MODULE?=on
OUTPUT?=./bin

.PHONY: vendor
vendor:
	GO111MODULE=${GO111MODULE} go get ./... && \
	GO111MODULE=${GO111MODULE} go mod tidy && \
	GO111MODULE=${GO111MODULE} go mod vendor

.PHONY: build
build:
	GO111MODULE=${GO111MODULE} GOOS=linux GOARCH=amd64 go build \
		-mod vendor \
		-o ${OUTPUT}/app cmd/recovery/main.go

.PHONY: image
image: vendor build
	docker build -t dtelepnev-recovery .

.PHONY: test
test:
	@echo "test"

.PHONY: lint
lint:
	@echo "lint"
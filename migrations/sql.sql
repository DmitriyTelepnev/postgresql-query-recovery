CREATE TABLE _downloaded_files (filename varchar(100), ts timestamp);
CREATE UNIQUE INDEX ON _downloaded_files (filename);
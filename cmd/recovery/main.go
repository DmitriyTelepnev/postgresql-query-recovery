package main

import (
	"sync"

	"bitbucket.org/DmitriyTelepnev/postgresql-query-recovery/internal/infrastructure/migrations"

	"bitbucket.org/DmitriyTelepnev/postgresql-query-recovery/internal/app"
	"bitbucket.org/DmitriyTelepnev/postgresql-query-recovery/internal/infrastructure/config"
	"bitbucket.org/DmitriyTelepnev/postgresql-query-recovery/internal/infrastructure/datasource"
	"bitbucket.org/DmitriyTelepnev/postgresql-query-recovery/internal/infrastructure/repository"
	"bitbucket.org/DmitriyTelepnev/postgresql-query-recovery/internal/infrastructure/service"
	"bitbucket.org/DmitriyTelepnev/postgresql-query-recovery/pkg/infrastructure/logger"
	"bitbucket.org/DmitriyTelepnev/postgresql-query-recovery/pkg/infrastructure/signal"
)

func main() {
	cfg := config.MustConfigure()
	log := logger.NewStdout(cfg.Log.Level)
	log.Infof("Started with cfg: %#v", cfg)

	mainWg := &sync.WaitGroup{}

	pgConn, pgConnErr := datasource.NewPostgresConn(cfg.PgConn)
	defer pgConn.Close()
	exitOnError(log, pgConnErr)

	migrations.MustUp(pgConn, log)

	unpackSvc := service.NewUnpack(cfg.UnpackedDirPath, log)
	publicTableRepo := repository.NewPublicTables(pgConn, log)
	downloadedFilesRepo := service.NewDownloadedFilesRepo(pgConn, log)

	recovery := app.NewRecoveryService(
		cfg.DownloadPeriod,
		cfg.SourceDirPath,
		unpackSvc,
		publicTableRepo,
		downloadedFilesRepo,
		log,
	)
	cleanupUnpacked := app.NewCleanupUnpacked(cfg.UnpackedDirPath, cfg.Lifetime, log)
	withWaitGroup(mainWg, recovery.Run, log)
	withWaitGroup(mainWg, cleanupUnpacked.Run, log)

	signalHandler := signal.NewHandler(func() error {

		err := recovery.Shutdown()
		if err != nil {
			exitOnError(log, err)
		}

		return cleanupUnpacked.Shutdown()
	})

	shutdownError := signalHandler.Poll()
	exitOnError(log, shutdownError)

	log.Info("service correctly stopped")

}

func withWaitGroup(wg *sync.WaitGroup, f func() error, logger logger.Logger) {
	wg.Add(1)
	go func() {
		defer wg.Done()
		exitOnError(logger, f())
	}()
}

func exitOnError(log logger.Logger, e error) {
	if e != nil {
		log.Fatalf("%s", e.Error())
	}
}

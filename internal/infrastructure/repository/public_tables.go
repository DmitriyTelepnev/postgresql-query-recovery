package repository

import (
	"database/sql"
	"fmt"

	"bitbucket.org/DmitriyTelepnev/postgresql-query-recovery/internal/domain/entities"
	"bitbucket.org/DmitriyTelepnev/postgresql-query-recovery/pkg/infrastructure/logger"
)

type PublicTables struct {
	conn   *sql.DB
	logger logger.Logger
}

func NewPublicTables(conn *sql.DB, logger logger.Logger) *PublicTables {
	return &PublicTables{conn: conn, logger: logger}
}

const saveTableData = "INSERT INTO %s SELECT %s, '%s' as recovered_from FROM json_populate_recordset(null::%s, $1) ON CONFLICT DO NOTHING"

func (r *PublicTables) Save(table *entities.Table, recoveredFrom string) error {
	columnNames := ""
	columnsRow := r.conn.QueryRow("SELECT string_agg(column_name, ',') FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = $1 AND column_name != 'recovered_from'", table.Name)
	err := columnsRow.Scan(&columnNames)
	if err != nil {
		return err
	}
	res, err := r.conn.Exec(fmt.Sprintf(saveTableData, table.Name, columnNames, recoveredFrom, table.Name), string(table.Data))
	r.logger.Debugf("Inserted %#v", res)
	return err
}

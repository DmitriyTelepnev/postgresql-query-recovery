package service

import (
	"database/sql"

	"bitbucket.org/DmitriyTelepnev/postgresql-query-recovery/pkg/infrastructure/logger"
)

type DownloadedFiles struct {
	conn   *sql.DB
	logger logger.Logger
}

func NewDownloadedFilesRepo(conn *sql.DB, logger logger.Logger) *DownloadedFiles {
	return &DownloadedFiles{conn: conn, logger: logger}
}

const checkIsDownloaded = "SELECT EXISTS(SELECT true FROM _downloaded_files WHERE filename = $1 LIMIT 1)"

func (s *DownloadedFiles) CheckIsDownloaded(filename string) (bool, error) {
	isExists := false
	row := s.conn.QueryRow(checkIsDownloaded, filename)

	err := row.Scan(&isExists)

	return isExists, err
}

const markAsDownloaded = "INSERT INTO _downloaded_files (filename, ts) VALUES ($1, now())"

func (s *DownloadedFiles) MarkAsDownloaded(filename string) error {
	res, err := s.conn.Exec(markAsDownloaded, filename)
	s.logger.Debugf("[MarkAsDownloaded] %#v", res)
	return err
}

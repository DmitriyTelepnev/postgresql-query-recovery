package service

import (
	"archive/tar"
	"bufio"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	"bitbucket.org/DmitriyTelepnev/postgresql-query-recovery/pkg/infrastructure/logger"
)

type Unpack struct {
	unpackDir string
	logger    logger.Logger
}

func NewUnpack(unpackDir string, logger logger.Logger) *Unpack {
	return &Unpack{
		unpackDir: unpackDir,
		logger:    logger,
	}
}

func (u *Unpack) Unpack(dirPath string, filename string) (string, error) {
	filePath := fmt.Sprintf("%s/%s", dirPath, filename)

	fi, err := os.Stat(filePath)
	if err != nil {
		return "", err
	}
	dirname := fmt.Sprintf("%s/%s", u.unpackDir, strings.Replace(fi.Name(), ".tar", "", 1))

	ft, err := os.Open(filePath)
	defer ft.Close()
	if err != nil {
		return "", err
	}

	if _, err := os.Stat(dirname); err != nil {
		if err := os.MkdirAll(dirname, 0755); err != nil {
			return "", err
		}
	}

	tr := tar.NewReader(bufio.NewReader(ft))

	for {
		header, err := tr.Next()

		switch {

		case err == io.EOF:
			return dirname, nil

		case err != nil:
			return "", err

		case header == nil:
			continue
		}

		target := filepath.Join(dirname, header.Name)

		switch header.Typeflag {

		case tar.TypeReg:
			f, err := os.Create(target)
			if err != nil {
				return "", err
			}

			if _, err := io.Copy(f, tr); err != nil {
				return "", err
			}

			f.Close()
		}
	}
	return "", nil
}

func (u *Unpack) IsTar(dirPath string, filename string) bool {
	filePath := fmt.Sprintf("%s/%s", dirPath, filename)
	return strings.Contains(filePath, ".tar")
}

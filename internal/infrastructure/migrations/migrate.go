package migrations

import (
	"database/sql"

	"bitbucket.org/DmitriyTelepnev/postgresql-query-recovery/pkg/infrastructure/logger"
)

const createDownloadedFiles = "CREATE TABLE IF NOT EXISTS _downloaded_files (filename varchar(100), ts timestamp);"
const createDownloadedFilesUniqueIndex = "CREATE UNIQUE INDEX IF NOT EXISTS  _downloaded_files_idx ON _downloaded_files (filename);"

func MustUp(pgConn *sql.DB, logger logger.Logger) {
	_, err := pgConn.Exec(createDownloadedFiles)
	panicOnErr(err)

	_, err = pgConn.Exec(createDownloadedFilesUniqueIndex)
	panicOnErr(err)
}

func panicOnErr(err error) {
	if err != nil {
		panic(err)
	}
}

package config

import (
	"fmt"
	"time"

	"github.com/spf13/viper"
)

type (
	PgConnection struct {
		Host     string
		Port     uint
		Dbname   string
		Username string
		Password string
	}
	Log struct {
		Level string
	}

	Root struct {
		DownloadPeriod  time.Duration
		Lifetime        time.Duration
		SourceDirPath   string
		UnpackedDirPath string
		PgConn          PgConnection
		Log             Log
	}
)

func MustConfigure() *Root {
	cfg := &Root{}

	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	err = viper.Unmarshal(cfg)
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	return cfg
}

func (c *PgConnection) GetHost() string {
	return fmt.Sprintf("%s:%d", c.Host, c.Port)
}

package app

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"time"

	"bitbucket.org/DmitriyTelepnev/postgresql-query-recovery/internal/domain/entities"

	"bitbucket.org/DmitriyTelepnev/postgresql-query-recovery/internal/domain/repository"
	"bitbucket.org/DmitriyTelepnev/postgresql-query-recovery/internal/infrastructure/service"
	"bitbucket.org/DmitriyTelepnev/postgresql-query-recovery/pkg/infrastructure/logger"
)

type recoverySvc struct {
	isStopped       chan bool
	ticker          *time.Ticker
	sourceDir       string
	unpackSvc       *service.Unpack
	publicTables    repository.PublicTables
	downloadedFiles *service.DownloadedFiles
	logger          logger.Logger
}

func NewRecoveryService(downloadPeriod time.Duration, sourceDir string, unpackSvc *service.Unpack, publicTables repository.PublicTables, downloadedFiles *service.DownloadedFiles, logger logger.Logger) *recoverySvc {
	ticker := time.NewTicker(downloadPeriod)
	return &recoverySvc{
		isStopped:       make(chan bool),
		ticker:          ticker,
		sourceDir:       sourceDir,
		unpackSvc:       unpackSvc,
		publicTables:    publicTables,
		downloadedFiles: downloadedFiles,
		logger:          logger,
	}
}

func (s *recoverySvc) Run() error {
	for {
		select {
		case <-s.isStopped:
			return nil
		case <-s.ticker.C:
			s.migrate()
		}
	}
}

func (s *recoverySvc) migrate() {
	fi, err := os.Stat(s.sourceDir)
	if err != nil || !fi.IsDir() {
		s.logger.Errorf("%s must be dir", s.sourceDir)
		return
	}

	fis, err := ioutil.ReadDir(s.sourceDir)
	if err != nil {
		s.logger.Error(err.Error())
		return
	}
	for _, fi := range fis {
		if s.unpackSvc.IsTar(s.sourceDir, fi.Name()) {
			isDownloaded, err := s.downloadedFiles.CheckIsDownloaded(fi.Name())
			if err != nil {
				s.logger.Error(err.Error())
				return
			}
			if !isDownloaded {
				path, err := s.unpackSvc.Unpack(s.sourceDir, fi.Name())
				if err != nil {
					s.logger.Error(err.Error())
					continue
				}

				err = s.migrateData(path)
				if err != nil {
					s.logger.Error(err.Error())
					continue
				}

				err = s.downloadedFiles.MarkAsDownloaded(fi.Name())
				if err != nil {
					s.logger.Error(err.Error())
					continue
				}

				err = os.RemoveAll(path)
				if err != nil {
					s.logger.Error(err.Error())
					continue
				}
			}
		}
	}
}

func (s *recoverySvc) migrateData(path string) error {
	fi, err := os.Stat(path)
	if err != nil || !fi.IsDir() {
		return s.logger.Errorf("%s must be dir", path)
	}

	fis, err := ioutil.ReadDir(path)
	if err != nil {
		return s.logger.Error(err.Error())
	}
	for _, fInfo := range fis {
		table := &entities.Table{Name: fInfo.Name()}
		table.Data, err = ioutil.ReadFile(filepath.Join(path, fInfo.Name()))
		if err != nil {
			return err
		}

		if len(table.Data) != 0 {
			err = s.publicTables.Save(table, fi.Name())
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (s *recoverySvc) Shutdown() error {
	s.isStopped <- true
	s.ticker.Stop()
	return nil
}

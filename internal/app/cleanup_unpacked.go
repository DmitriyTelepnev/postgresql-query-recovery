package app

import (
	"io/ioutil"
	"os"
	"time"

	"bitbucket.org/DmitriyTelepnev/postgresql-query-recovery/pkg/infrastructure/logger"
)

type CleanupUnpacked struct {
	unpackedDirPath string
	isStopped       chan bool
	ttl             time.Duration
	ticker          *time.Ticker
	logger          logger.Logger
}

func NewCleanupUnpacked(unpackedDirPath string, ttl time.Duration, logger logger.Logger) *CleanupUnpacked {
	ticker := time.NewTicker(ttl)

	return &CleanupUnpacked{
		unpackedDirPath: unpackedDirPath,
		isStopped:       make(chan bool),
		ticker:          ticker,
		logger:          logger,
	}
}

func (s *CleanupUnpacked) Run() error {
	for {
		select {
		case <-s.isStopped:
			return nil
		case <-s.ticker.C:
			s.cleanDir()
		}
	}
}

func (s *CleanupUnpacked) cleanDir() {
	fi, err := os.Stat(s.unpackedDirPath)
	if err != nil || !fi.IsDir() {
		s.logger.Errorf("%s must be dir", s.unpackedDirPath)
		return
	}

	fis, err := ioutil.ReadDir(s.unpackedDirPath)
	if err != nil {
		s.logger.Error(err.Error())
		return
	}
	for _, fi := range fis {
		if time.Since(fi.ModTime()) > s.ttl {
			err = os.Remove(fi.Name())
			if err != nil {
				s.logger.Error(err.Error())
				continue
			}
		}
	}
}

func (s *CleanupUnpacked) Shutdown() error {
	s.isStopped <- true
	s.ticker.Stop()
	return nil
}

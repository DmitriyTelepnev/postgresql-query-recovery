package entities

type Table struct {
	Name string
	Data []byte
}

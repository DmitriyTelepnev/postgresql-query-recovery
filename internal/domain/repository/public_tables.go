package repository

import "bitbucket.org/DmitriyTelepnev/postgresql-query-recovery/internal/domain/entities"

type PublicTables interface {
	Save(table *entities.Table, recoveredFrom string) error
}

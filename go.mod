module bitbucket.org/DmitriyTelepnev/postgresql-query-recovery

go 1.12

require (
	github.com/lib/pq v1.5.2
	github.com/pkg/errors v0.9.1
	github.com/spf13/viper v1.7.0
)
